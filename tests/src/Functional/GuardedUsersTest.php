<?php

namespace Drupal\Tests\guardian\Functional;

use Drupal\Core\Site\Settings;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests Guardian protects Guarded users.
 *
 * @group guardian
 */
class GuardedUsersTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  use AssertMailTrait {
    getMails as drupalGetMails;
  }

  /**
   * A guarded user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $guardedUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $settings['settings']['guardian_mail'] = (object) [
      'value' => 'bob@example.com',
      'required' => TRUE,
    ];
    $this->writeSettings($settings);
    $this->guardedUser = User::load(1);
    $this->guardedUser->setPassword('drupal');
    $this->guardedUser->passRaw = 'drupal';
    $this->guardedUser->save();

    $admin_role = $this->createAdminRole('administrator');
    $this->adminUser = $this->drupalCreateUser([], 'jim', FALSE, ['roles' => $admin_role]);
  }

  /**
   * Tests Guarded users are protected.
   */
  public function testGuardedUserProtection() {
    $this->assertInstanceOf(UserInterface::class, $this->guardedUser);
    $this->assertEquals(1, $this->guardedUser->id());
    $this->assertEquals('admin@example.com', $this->guardedUser->getEmail());
    $this->assertEquals('bob@example.com', Settings::get('guardian_mail'));
    $this->drupalLogin($this->guardedUser);

    // Install the guardian module.
    $edit_enable = [
      'modules[guardian][enable]' => TRUE,
    ];
    $this->drupalGet('admin/modules');
    $this->submitForm($edit_enable, 'Install');
    $this->rebuildContainer();
    $this->resetAll();

    // Verify that the user was sent an email.
    $this->assertMail('to', 'bob@example.com');
    $subject = t('Guardian has been enabled for @site', ['@site' => $this->config('system.site')->get('name')]);
    $this->assertMail('subject', $subject);

    // Check the guarded user profile fields are disabled.
    $this->drupalGet('user/1/edit');
    // Check the guarded user email address was updated.
    $this->guardedUser = User::load(1);
    $this->assertEquals('bob@example.com', $this->guardedUser->getEmail());
    $mail = $this->assertSession()->elementExists('css', '#edit-mail');
    $mail->hasAttribute('disabled');
    $username = $this->assertSession()->elementExists('css', '#edit-name');
    $username->hasAttribute('disabled');
    $this->assertSession()->checkboxChecked('edit-status-1');

    // Check a guarded user is automatically logged out.
    $this->guardedUser->setLastAccessTime(strtotime('-3 hours'))->save();
    $this->drupalGet('<front>');
    $this->assertSession()->responseContains('Your last access was more than 2 hours ago, please login again.');
    $this->assertFalse($this->drupalUserIsLoggedIn($this->guardedUser));

    // Check a guarded user can log in using the password reset email.
    $this->drupalGet('user/password');
    $this->submitForm(['name' => $this->guardedUser->getAccountName()], 'Submit');
    $reset_url = $this->getResetUrl();
    $this->drupalGet($reset_url . '/login');
    $this->assertSession()->responseContains('You have just used your one-time login link. It is no longer necessary to use this link to log in.');

    $this->assertEqualsCanonicalizing([(int) $this->guardedUser->id()], \Drupal::service('guardian.manager')->getGuardedUids());
    \Drupal::service('module_installer')->install(['guardian_test']);
    \Drupal::service('guardian.manager')->resetGuardedUsers();
    $this->assertEqualsCanonicalizing([(int) $this->guardedUser->id(), (int) $this->adminUser->id()], \Drupal::service('guardian.manager')->getGuardedUids());
  }

  /**
   * Retrieves password reset email and extracts the login link.
   */
  public function getResetUrl() {
    // Assume the most recent email.
    $_emails = $this->drupalGetMails();
    $email = end($_emails);
    $urls = [];
    preg_match('#.+user/reset/.+#', $email['body'], $urls);

    return $urls[0];
  }

}
