<?php

/**
 * @file
 * API documentation for the Guardian module.
 */

/**
 * Alter the Guardian mail metadata, that will be appended to the body text.
 *
 * @param string[] $body
 *   Content of mail body.
 */
function hook_guardian_add_metadata_to_body_alter(array &$body) {
  if (!empty($_SERVER['HTTP_USER_AGENT'])) {
    $body[] = t('HTTP_USER_AGENT: @user_agent', ['@user_agent' => $_SERVER['HTTP_USER_AGENT']]);
  }
}

/**
 * Provides additional guarded users.
 *
 * @return string[]
 *   A list of guarded user email addresses keyed by user id.
 */
function hook_guardian_guarded_users(): array {
  $guarded_users = [];
  /** @var \Drupal\user\UserInterface[] $administrator_users */
  $administrator_users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties([
    'roles' => 'administrator',
  ]);
  foreach ($administrator_users as $user_id => $administrator_user) {
    $guarded_users[$user_id] = $administrator_user->getEmail();
  }
  return $guarded_users;
}
