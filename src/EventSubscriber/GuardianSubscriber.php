<?php

namespace Drupal\guardian\EventSubscriber;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\guardian\GuardianManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class guardianSubscriber.
 *
 * @package Drupal\guardian\EventSubscriber
 */
class GuardianSubscriber implements EventSubscriberInterface {

  /**
   * GuardianSubscriber constructor.
   */
  public function __construct(protected GuardianManagerInterface $guardianManager, protected AccountProxyInterface $currentUser) {}

  /**
   * Returns password reset page if the current Guarded User is invalid.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function checkUser(RequestEvent $event) {
    $current_path = $event->getRequest()->getPathInfo();

    // Show message to guarded users that are logged out with force.
    $guardian_message = $event->getRequest()->get('guardian_redirect', 0);
    if ($guardian_message) {
      $this->guardianManager->showLogoutMessage();
      return;
    }

    // Skip user/reset url token.
    if (strpos($current_path, '/user/reset') === 0) {
      return;
    }

    // Skip non-guarded users.
    if (!$this->guardianManager->isGuarded($this->currentUser)) {
      return;
    }

    // Skip valid guarded users.
    if ($this->guardianManager->hasValidSession($this->currentUser) && $this->guardianManager->hasValidData($this->currentUser)) {
      return;
    }

    // Destroy session and redirect to password reset.
    $this->guardianManager->destroySession($this->currentUser);

    $password_url = Url::fromRoute('user.pass');
    $password_url->setRouteParameters([
      'destination' => $current_path,
      'guardian_redirect' => 1,
    ]);

    $response = new RedirectResponse($password_url->toString());
    $event->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkUser', 50];
    return $events;
  }

}
